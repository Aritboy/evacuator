'use strict';

let gulp                   = require('gulp'),
    sass                   = require('gulp-sass'),
    concat                 = require('gulp-concat'),
    uglify                 = require('gulp-uglify-es').default,
    cssnano                = require('gulp-cssnano'),
    autoprefixer           = require('gulp-autoprefixer'),
    sourcemaps             = require('gulp-sourcemaps'),
    gutil                  = require('gulp-util'),
    stripCssComments       = require('gulp-strip-css-comments'),
    strip                  = require('gulp-strip-comments'),
    pug                    = require('gulp-pug'),
    style_lint             = require('gulp-stylelint'),
    pugLinter              = require('gulp-pug-linter'),
    include                = require('gulp-include'),
    eslint                 = require('gulp-eslint');


const dist = './dist';
const src = './src';
const vendor = './src/vendor'


const path = {
  src: {
    js: {
      dir: `${src}/js`,
      concat: [
        `${src}/js/index.js`,
        `./node_modules/remodal/dist/remodal.js`,
        `${vendor}/slick-1.8.1/slick/slick.js`,
        `./node_modules/@fancyapps/fancybox/dist/jquery.fancybox.js`,
      ]
    },
    scss: {
      dir: `${src}/scss`,
      files: [
        `${src}/scss/main.scss`
      ]
    },
    img: {
      dir: `${src}/images`,
      files: [
        `${src}/images/**/*.+(png|jpg|jpeg|svg)`
      ]
    },
    view: {
      dir: `${src}/view`,
      files: [
        `${src}/view/**/*.+(pug)`
      ]
    },
    fonts: [
      `${vendor}/slick-1.8.1/slick/fonts/*.+(eot|svg|ttf|woff|woff2|otf)`,
      `${src}/fonts/**/*.+(eot|svg|ttf|woff|woff2|otf)`,
    ],
  },
  dist: {
    js: `${dist}/js`,
    css: `${dist}/css`,
    fonts: `${dist}/fonts`,
    view: `${dist}/`,
    img: `${dist}/images`
  },
  watch: {
    scss: [
      `${src}/scss/**/*.scss`
    ],
    view: [
      `${src}/view/**/*.+(html|pug)`
    ],
    js: [
      `${src}/js/**/*.js`,
    ],
  },
  lint: {
    js: `${src}/js/**/*.js`,
    scss: `${src}/scss/**/*.scss`,
    view: `${src}/view/**/*.+(pug|jade|html)`
  }
}


/**
 * Компиляция файлов SCSS в один общий.
 */
function compile_sass() {
  console.log('Начало компиляция scss.');
  return gulp.src(
    path.src.scss.files
  )
    .pipe(gutil.env.type_build === 'production' ? gutil.noop() : sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(gutil.env.type_build === 'production' ? stripCssComments() : gutil.noop())
    .pipe(gutil.env.type_build === 'production' ? cssnano() : gutil.noop())
    .pipe(gutil.env.type_build === 'production' ? autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {cascade: true}) : gutil.noop())
    .pipe(gutil.env.type_build === 'production' ? gutil.noop() : sourcemaps.write('.'))
    .pipe(gulp.dest(
      path.dist.css
    ))
}


/**
 * Удаление комментариев, соединение скриптов вместе, создание карты и сжатие js файлов.
 */
function scripts() {
  console.log('Удаление комментариев, соединение скриптов вместе, создание карты и сжатие js файлов.');
  return gulp.src(
    path.src.js.concat
  )
    .pipe(include())
    // .pipe(strip({
    //   ignore: /\/\/=include/
    // })) // вырезает комментарии
    .pipe(sourcemaps.init())
    .pipe(concat('index.js')) // имя результирующего объединенного файла
    .pipe(gutil.env.type_build === 'production' ? uglify() : gutil.noop())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(
      path.dist.js
    ));
}


/**
 * Перемещение файлов шрифтов в директорию сборки.
 */
function moveFonts() {
  console.log('Перемещение файлов шрифтов в директорию сборки.');
  return gulp.src(
    path.src.fonts
  ).pipe(gulp.dest(
    path.dist.fonts
  ));
}


function moveImages() {
  console.log('Перемещение изображений в dist');
  return gulp.src(
    path.src.img.files
  ).pipe(gulp.dest(
    path.dist.img
  ));
}


function view() {
  console.log('PUG compile')
  return gulp.src(path.src.view.files)
    .pipe(pug({
      doctype: 'html',
      pretty: true // Можно поставить false, для минимизации html.
    }))
    .pipe(gulp.dest(`${path.dist.view}`));
}


function lint_scss() {
  console.log('SCSS lint');
  return gulp.src(path.lint.scss)
    .pipe(style_lint({
      failAfterError: !!gutil.env.lint_break,
      reporters: [{
        formatter: 'string',
        console: true,
      },
      ],
    }));
}


function lint_pug() {
  console.log('Pug lint');
  return gulp.src(path.lint.view)
    .pipe(pugLinter({
        failAfterError: !!gutil.env.lint_break,
        reporter: 'default',
      }
    ));
}


function lint_js() {
  console.log('JS lint');
  return gulp.src(path.lint.js)
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(gutil.env.lint_break === true ? eslint.failAfterError() : gutil.noop());
}


/**
 * Наблюдение за изменением файлов и перезапуск задачи в случае их изменения.
 */
function watch() {
  gulp.watch(path.watch.js, scripts);
  gulp.watch(path.watch.scss, compile_sass);
  gulp.watch(path.watch.view, view);
  gulp.watch(path.watch.scss, lint_scss);
  gulp.watch(path.watch.view, lint_pug);
  gulp.watch(path.watch.js, lint_js);
}


exports.scripts = scripts;
exports.watch = watch;
exports.view_compile = view;
exports.img_exports = moveImages;
exports.scss_compile = compile_sass;
exports.lint_scss = lint_scss;
exports.lint_pug = lint_pug;
exports.lint_view = lint_pug;
exports.lint_js = lint_js;


exports.default = gulp.series(
  gulp.parallel(
    scripts,
    compile_sass,
    moveFonts,
    view,
    lint_pug,
    lint_scss,
    lint_js,
    moveImages,
  ),
  watch,
);
