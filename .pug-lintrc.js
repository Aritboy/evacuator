module.exports = {
    "extends": '@gabegabegabe/pug-lint-config', // общие базовые стили из npm пакета
    "disallowMultipleLineBreaks": null, // несколько пустых строк
    "validateIndentation": 4, // 4 пробела для отступа блока
    "validateExtensions": null, // позволяет не указывать при include расширение файла
    "maximumLineLength": null, // убирает ограничение на текстовую длину контента,
    "requireLowerCaseAttributes": null, // снимает требование к записи атрибутов в lower case
    "validateAttributeQuoteMarks": null, // без этого ругается на link в head
    "validateLineBreaks": null,
    "disallowHtmlText": null,
    "disallowAttributeConcatenation": null,
    "requireSpecificAttributes": null
};
